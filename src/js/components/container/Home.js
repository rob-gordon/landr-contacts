import React, { Component } from 'react';
import PropTypes from 'prop-types';
import 'regenerator-runtime/runtime';
import Main from '../presentational/Main';
import Contacts from '../presentational/Contacts';
import NoContacts from '../presentational/NoContacts';

/**
 * The home screen when the user arrives
 *
 * Displays filterable / sortable list
 * of all contacts
 */
class Home extends Component {
  static propTypes = {
    /** Function to create new contact */
    createContact: PropTypes.func.isRequired,
    /** Currently searched term */
    searchTerm: PropTypes.string.isRequired,
    /** Function to update search term */
    searchContacts: PropTypes.func.isRequired,
    /** List of Contacts */
    contacts: PropTypes.array.isRequired,
    /** Type of ordering for contacts */
    orderBy: PropTypes.string.isRequired,
    /** Reacter Router history object */
    history: PropTypes.object.isRequired,
    /** Function to update orderBy term */
    changeOrder: PropTypes.func.isRequired,
  };

  render() {
    const { searchTerm, createContact, searchContacts } = this.props;
    let { contacts } = this.props;
    if (searchTerm !== '') {
      contacts = contacts.filter(
        contact => contact.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1,
      );
    }
    if (this.props.orderBy === 'Name') {
      contacts.sort((a, b) => {
        const aLastName = a.name.split(' ')[a.name.split(' ').length - 1];
        const bLastName = b.name.split(' ')[b.name.split(' ').length - 1];
        return aLastName.localeCompare(bLastName);
      });
    } else if (this.props.orderBy === 'Date') {
      contacts.sort((a, b) => {
        return b.dateModified - a.dateModified;
      });
    }
    return (
      <div className="home">
        <nav>
          <button
            onClick={async () => {
              const id = await createContact();
              console.log(id);
              this.props.history.push(`/edit/${id}`);
            }}
          >
            Add Contact
          </button>

          <input
            type="text"
            placeholder="Search"
            value={searchTerm}
            onChange={e => {
              searchContacts(e.target.value);
            }}
          />

          <div className="order-by">
            {['Name', 'Date'].map(order => (
              <label key={order}>
                <input
                  type="radio"
                  name="order"
                  value={order}
                  checked={this.props.orderBy === order}
                  onChange={() => {
                    this.props.changeOrder(order);
                  }}
                />
                <span>{order}</span>
              </label>
            ))}
          </div>
        </nav>
        <Main>
          {contacts.length ? (
            <Contacts orderBy={this.props.orderBy} contacts={contacts} />
          ) : (
            <NoContacts />
          )}
        </Main>
      </div>
    );
  }
}

export default Home;
