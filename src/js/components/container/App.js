import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Loading from '../presentational/Loading';
import Home from './Home';
import ContactDetails from './ContactDetails';

const axios = require('axios');
const clone = require('clone-deep');
const uid = require('uuid/v1');

/**
 * Main Application
 */
export default class App extends Component {
  state = {
    /** The list of contacts */
    contacts: [],
    /** Whether the application is loading */
    loading: true,
    /** The string that contacts are filtered by when searching. */
    searchTerm: '',
    /** The order that contacts are displayed, Name|Date */
    orderBy: 'Name',
  };

  /**
   * Creates a new contact
   */
  createContact = data => {
    return new Promise((res, rej) => {
      let id = uid();
      const _data = typeof data !== 'undefined' ? data : { name: '' };
      _data.id = id;
      _data.dateModified = Date.now();
      this.saveContact(_data, res.bind(this, id));
    });
  };

  /**
   * Deletes the contact that matches the id passed in
   */
  deleteContact = id => {
    const { contacts } = this.state;
    if (contacts.map(contact => contact.id).indexOf()) {
      if (window.confirm(`Delete ${contacts.filter(contact => contact.id === id)[0].name}?`)) {
        return new Promise((resolve, rej) => {
          let newContacts = clone(contacts);
          const deleteAtIndex = newContacts.findIndex(contact => contact.id === id);

          newContacts.splice(deleteAtIndex, 1);
          axios({
            method: 'put',
            url: 'https://landr-contacts.firebaseio.com/contacts.json',
            data: newContacts,
          })
            .then(response => {
              this.setState({ contacts: newContacts }, () => {
                this.getContacts();
              });
              resolve(true);
            })
            .catch(error => {
              console.error(error);
              rej(false);
            });
        });
      }
    }
    return false;
  };

  /**
   * Updates the data associated with a contact
   */
  saveContact = (data, callback = () => {}) => {
    const { contacts } = this.state;
    const updatedContacts = clone(contacts);
    const index = updatedContacts.findIndex(contact => contact.id === data.id);
    if (index !== -1) {
      updatedContacts[index] = data;
    } else {
      updatedContacts.push(data);
    }

    axios({
      method: 'put',
      url: 'https://landr-contacts.firebaseio.com/contacts.json',
      data: updatedContacts,
    })
      .then(res => {
        this.setState({ contacts: updatedContacts }, () => {
          if (typeof callback === 'function') {
            callback();
          }
          this.getContacts();
        });
      })
      .catch(error => {
        console.error(error);
      });
  };

  /**
   * Sets the searchTerm to filter the contact list
   */
  searchContacts = term => {
    this.setState({ searchTerm: term });
  };

  getContacts = () => {
    axios
      .get('https://landr-contacts.firebaseio.com/contacts.json')
      .then(res => {
        const { data } = res;
        this.setState({ loading: false, contacts: clone(data) });
      })
      .catch(error => {
        console.error(error);
      });
  };

  /**
   * Changes the orderBy state
   */
  changeOrder = orderBy => {
    this.setState({ orderBy });
  };

  /**
   * make ajax request for contacts when component loads
   * */
  componentDidMount() {
    this.getContacts();
  }

  render() {
    const { loading } = this.state;
    return (
      <Router>
        <div className="App">
          <h1>Contacts</h1>
          {loading ? (
            <Loading />
          ) : (
            <Switch>
              <Route
                exact
                path="/"
                render={props => {
                  return (
                    <Home
                      {...this.state}
                      {...props}
                      changeOrder={this.changeOrder}
                      createContact={this.createContact}
                      searchContacts={this.searchContacts}
                    />
                  );
                }}
              />
              <Route
                path={'/edit/:id'}
                render={props => {
                  return (
                    <ContactDetails
                      deleteContact={this.deleteContact}
                      saveContact={this.saveContact}
                      {...this.state}
                      {...props}
                      editing={true}
                    />
                  );
                }}
              />
              <Route
                path={'/:id'}
                render={props => {
                  return (
                    <ContactDetails
                      deleteContact={this.deleteContact}
                      saveContact={this.saveContact}
                      {...this.state}
                      {...props}
                    />
                  );
                }}
              />
            </Switch>
          )}
        </div>
      </Router>
    );
  }
}
