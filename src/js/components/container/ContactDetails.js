import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Main from '../presentational/Main';
import CurrentContact from './CurrentContact';
import NoContact from '../presentational/NoContact';

const clone = require('clone-deep');

/**
 * Displays information for the selected contact
 *
 * Has the ability to update contact details and
 * delete the contact
 */
export default class ContactDetails extends Component {
  static propTypes = {
    /** list of contacts */
    contacts: PropTypes.array.isRequired,
    /** Whether the user is editing the contact info */
    editing: PropTypes.bool,
    /** Method to save updates to contact info */
    saveContact: PropTypes.func.isRequired,
    /** Method to delete contact */
    deleteContact: PropTypes.func.isRequired,
    /** History object from React Router */
    history: PropTypes.object.isRequired,
  };

  state = {
    /** The information for the contact currently beind edited
     *
     * Contact info is cloned locally to avoid making any changes
     * to state without the user explicitly saving changes
     */
    currentContact: clone(
      (this.props.contacts.filter(contact => contact.id === this.props.match.params.id) || [
        false,
      ])[0],
    ),
  };

  /**
   * Update the current *local* contact info
   */
  updateCurrentContact = (slug, value) => {
    const { currentContact } = this.state;
    currentContact[slug] = value;
    this.setState({ currentContact });
  };

  render() {
    const { currentContact } = this.state;
    const editing = this.props.editing || false;
    return (
      <div className="contact-details">
        <nav>
          <Link
            to={'/'}
            onClick={e => {
              if (editing && !!currentContact) {
                if (!window.confirm('Return to Contacts without Saving?')) {
                  e.preventDefault();
                  return false;
                }
              }
            }}
          >
            Back to Contacts
          </Link>
          {currentContact ? (
            <div className="contact-options">
              {!editing ? (
                <Link
                  to={`/edit/${currentContact.id}`}
                  onClick={e => {
                    setTimeout(() => {
                      document.querySelector('.name input').focus();
                    }, 50);
                  }}
                >
                  Edit
                </Link>
              ) : (
                <Link
                  to={`/${currentContact.id}`}
                  onClick={() => {
                    this.props.saveContact(currentContact);
                  }}
                >
                  Save
                </Link>
              )}

              <button
                onClick={async () => {
                  const deleted = await this.props.deleteContact(currentContact.id);
                  if (deleted) {
                    this.props.history.push(`/`);
                  }
                }}
              >
                Delete
              </button>
            </div>
          ) : (
            ''
          )}
        </nav>
        <Main>
          {currentContact ? (
            <CurrentContact
              currentContact={currentContact}
              history={this.props.history}
              editing={editing}
              updateCurrentContact={this.updateCurrentContact}
            />
          ) : (
            <NoContact />
          )}
        </Main>
      </div>
    );
  }
}
