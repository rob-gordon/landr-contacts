import React, { Component } from 'react';
import Label from '../presentational/Label';
import PropTypes from 'prop-types';

const clone = require('clone-deep');

/**
 * The form to edit details for the currently selected contact
 */
export default class CurrentContact extends Component {
  static propTypes = {
    updateCurrentContact: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    currentContact: PropTypes.object.isRequired,
  };

  /** Keyboard helpers to leave from current contact
   * back to contact list
   */
  keyListener = e => {
    if (e.key === 'Escape') {
      switch (document.activeElement.nodeName) {
        case 'INPUT':
          document.activeElement.blur();
          break;
        default:
          if (!this.props.editing) {
            this.props.history.push('/');
          }
      }
    }
  };

  componentDidMount() {
    // Select first input if new contact
    if (this.props.editing) {
      setTimeout(() => {
        document.querySelector('.name input').focus();
      }, 50);
    }

    // Attach key bindings
    document.addEventListener('keydown', this.keyListener, false);
  }

  componentWillUnmount() {
    // Dettach key bindings
    document.removeEventListener('keydown', this.keyListener);
  }

  render() {
    const { updateCurrentContact } = this.props;

    const { address, email, jobTitle, name, phoneNumber } = this.props.currentContact;

    const controls = [
      {
        name: 'Name',
        slug: 'name',
        type: 'text',
        value: name,
      },
      {
        name: 'Phone',
        slug: 'phoneNumber',
        type: 'tel',
        value: phoneNumber,
      },
      {
        name: 'Email',
        slug: 'email',
        type: 'email',
        value: email,
      },
      {
        name: 'Job Title',
        slug: 'jobTitle',
        type: 'text',
        value: jobTitle,
      },
      {
        name: 'Address',
        slug: 'address',
        value: address,
      },
    ];
    return (
      <div className={`current-contact ${this.props.editing ? 'editing' : ''}`}>
        <form
          onSubmit={e => {
            console.log('Submitted Form');
          }}
        >
          {controls.map(control => (
            <Label
              key={control.name}
              {...control}
              updateCurrentContact={updateCurrentContact}
              editing={this.props.editing}
            />
          ))}
        </form>
      </div>
    );
  }
}
