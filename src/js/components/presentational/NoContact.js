import React from 'react';

const NoContact = () => <div className="no-contact">Contact Not Found</div>;

export default NoContact;
