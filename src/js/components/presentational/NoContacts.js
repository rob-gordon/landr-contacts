import React from 'react'

const NoContacts = props => {
  return <div className="no-contacts">No Contacts Found</div>
}

export default NoContacts
