import React from 'react';

const slugify = require('slugify');

const Label = (props) => {
  const {
    name, slug, value, editing, updateCurrentContact, pattern,
  } = props;
  return (
    <div className={slugify(name).toLowerCase()}>
      <label htmlFor={name}>
        <span className="name">{name}</span>
        <input
          type="text"
          className="value"
          value={value}
          name={name}
          pattern={pattern || undefined}
          readOnly={!editing}
          onChange={(e) => {
            updateCurrentContact(slug, e.target.value);
          }}
        />
      </label>
    </div>
  );
};

export default Label;
