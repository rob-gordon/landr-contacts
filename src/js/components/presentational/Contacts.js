import React from 'react';
import { Link } from 'react-router-dom';

const Contacts = (props) => {
  const { contacts } = props;
  return (
    <div className="contacts-list">
      <ul>
        {contacts.map(contact => (
          <li key={contact.id}>
            <Link to={`/${contact.id}`}>{contact.name || '(No Name)'}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Contacts;
