import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/components/container/App';
import './scss/style.scss';

ReactDOM.render(<App />, document.getElementById('app'));
